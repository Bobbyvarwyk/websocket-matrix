const WebSocket = require('ws');
var url = require('url');

const wss = new WebSocket.Server({ port: 3030 });

// Define Object with responses for Front-end
let responses = {
    "accessible": { status: 1, message: "Customer limit under Treshold" }, // Zone is accesible
    "semicrowded": { status: 2, message: "Customer limit over Treshold" }, // Amount of people in zone is over half of the maximum allowed, but not at the maximum
    "crowded": { status: 3, message: "Customer limit reached" }, // Amount of people in the zone is at it's limit
    "overcrowded": { status: 4, message: "Customer limit exceeded" }, // Amount of people in the zone is over it's limit
    "error": {status: 5, message: "Error, input compromised" } // Error handling
}

let infoScreenOptions = {
    "options": {
        "massMultiplier": 1,
        "availableCarts": 30
    },
    "stats": {
        "registers": 3,
        "routes": true
    }
}

// Check if connection is opened and log all messages
wss.on('connection', (ws, req) => {
    // Save ID, camera and treshold to client on connection
    SaveParamsToClient(ws, req.url);

    // whenever a screen connects, send global options to it
    wss.clients.forEach(function each(client){
        if(client.type === 'infoOutput' && ws.ID === client.ID){
            client.send(JSON.stringify(infoScreenOptions))
        }
    })
    // When Message is received from camera's. Send it to front-end
    ws.on('message', data => {
        wss.clients.forEach(function each(client) {
            if(ws.readyState === WebSocket.OPEN && client.ID === ws.ID) {
                sendMessageToClients(ConvertToJSON(data).customerCount, client.ID);
            }
            // If the message is sent from a camera and if it's own source,
            if(client.type === 'input' && ws.ID === client.ID){
                let cameraID = client.ID
                // create message to send to screens
                let message = ConvertToJSON({"camera": client.ID, "customerCount": ConvertToJSON(data).customerCount})

                // send message to screens
                wss.clients.forEach(function each(client){
                    // if the type of connection is an info screen output, send the message to it
                    if(client.type === 'infoOutput'){
                        client.send(JSON.stringify(message))
                    }

                    // if the type of connection is an framing screen output and the camera ID matches, send 1 camera to the specified screen
                    if(client.type === 'framingOutput' && client.cameraID === cameraID){
                        client.send(JSON.stringify(message))
                    }
                })
            }
        });
    })
})

// Save ID, Treshold and cameraID in client instance based on URL parameters
function SaveParamsToClient(ws, URL) {
    if(ws === null) {
        return false;
    }

    if(URL === "" || URL === "/") {
        return false;
    }

    // Get individual parameters from URL
    const { query: {ID, type, treshold, cameraID } } = url.parse(URL, true);
    // Set the values for the client
    if(ID !== undefined) {
        ws.ID = ID;
    }

    if(type !== undefined) {
        ws.type = type;
    }

    if(treshold !== undefined) {
        ws.treshold = treshold;
    }

    if(cameraID !== undefined) {
        ws.cameraID = cameraID;
    }

    return true;
}

// Convert the messages from the clients to JSON
function ConvertToJSON(dataInput) {
    // Check if dataInput already is a JSON string, if not convert it
    dataInput = typeof dataInput !== "string" ? JSON.stringify(dataInput) : dataInput;

    try {
        dataInput = JSON.parse(dataInput);
    } catch (e) {
        console.error(e);
        return false;
    }

    if(typeof dataInput === "object" && dataInput !== null) {
        return dataInput;
    }

    return false;
}


// Calculate halfway treshold for status 2
function calculateHalfCustomerTreshold(customerTreshold) {
    customerTreshold = typeof customerTreshold !== "number" ? parseInt(customerTreshold) : customerTreshold;

    try {
        customerTreshold = customerTreshold / 2;
    } catch (e) {
        console.error(e);
        return false;
    }

    if(typeof customerTreshold === "number" && customerTreshold !== null) {
        return customerTreshold;
    }

    return false;
}

// Get the correct response message
function getResponseMsg(scannedCustomers, clientTreshold) {
    var responseMsg = "";

    var maxCustomers = parseInt(clientTreshold);
    var customerTreshold = calculateHalfCustomerTreshold(clientTreshold);

    if(scannedCustomers <= customerTreshold) {
        responseMsg = responses.accessible;
    }

    if(scannedCustomers >= customerTreshold) {
        responseMsg = responses.semicrowded;
    }

    if(scannedCustomers == maxCustomers) {
        responseMsg = responses.crowded;
    }

    if(scannedCustomers > maxCustomers) {
        responseMsg = responses.overcrowded;
    }

    if(responseMsg === "") {
        responseMsg = responses.error;
    }

    // Convert to JSON String
    return JSON.stringify(responseMsg);
}

// Send message to matching ID clients
function sendMessageToClients(data, clientID) {
    wss.clients.forEach(function each(client) {
        if (client.cameraID === clientID && client.readyState === WebSocket.OPEN) {
            client.send(getResponseMsg(data, client.treshold));
        }
    });
}
console.log("Started websocket at: " + wss.options.port);
